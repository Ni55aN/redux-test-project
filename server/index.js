const express = require('express');
const bodyParser = require('body-parser')
const passport = require('passport')
const app = express();
const {User, Event} = require('./db');
const config = require('./config');

app.use(bodyParser.json())
app.use(passport.initialize());

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    next();
});

app.use('/api', require('./auth-check'));
passport.use('signup', require('./passport/signup'));
passport.use('login', require('./passport/login'));

app.use('/auth', require('./routes/auth'));

app.get('/api', (req, res) => {
    Event.find({ userId: req.user.id }, (err, userEvents) => {
        res.json({ status: true, events: userEvents });
    });
});

app.post('/api/event', (req, res) => {
    var userId = req.user.id;
    var title = req.body.title;
    var start = req.body.start;
    var duration = req.body.duration;

    var fullTime = (config.endTime - config.startTime) * 60;

    var errors = [];

    if (typeof title === 'undefined' ||
        typeof start === 'undefined' ||
        typeof duration === 'undefined')
        errors.push('Not all required fields are present');
    
    if (typeof title !== 'string' ||
        typeof start !== 'number' ||
        typeof duration !== 'number')
        errors.push('Wrong types of data');
    
    if (title.length<5)
        errors.push('The title should be more informative');
    
    if (start < 0 || start + duration > fullTime)
        errors.push('Wrong start time or duration');
        
    if (errors.length>0) {
        res.status(400).json({ status: false, message: 'wrong data', errors});
        return;
    }

    var event = new Event({ userId, title, start, duration});

    event.save((err, e) => {
        if (err)
            res.json({ status: false, message: 'Error while saving event' })
        else
            res.json({ status: true, event: e });
    });
    
});

app.delete('/api/event/:id', (req, res) => {
    
    Event.findOne({ _id: req.params.id }).remove((err) => {
        if (err)
            res.json({ status: false })
        else 
            res.json({ status: true })
    });
});

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}!`);
});