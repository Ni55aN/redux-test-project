const jwt = require('jsonwebtoken');
const User = require('mongoose').model('User');
const config = require('./config');

module.exports = (req, res, next) => {
    const token = req.headers.authorization;
    
    if (!token) return res.status(401).end();

    return jwt.verify(token, config.jwtSecret, (err, decoded) => {
    
        if (err) { return res.status(401).end(); }

        const userId = decoded.sub;

        return User.findById(userId, (userErr, user) => {
            if (userErr || !user) {
                return res.status(401).end();
            }
            req.user = user;

            return next();
        });
    });
};