var mongoose = require('mongoose');
const bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        index: { unique: true },
        trim: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.methods.comparePassword = function comparePassword(password, callback) {
    bcrypt.compare(password, this.password, callback);
};
  
UserSchema.pre('save', function saveHook(next) {
    const user = this;
  
    if (!user.isModified('password')) return next();
  
    return bcrypt.genSalt((saltError, salt) => {
        if (saltError) { return next(saltError); }
  
        return bcrypt.hash(user.password, salt, (hashError, hash) => {
            if (hashError) { return next(hashError); }
  
            user.password = hash;
  
            return next();
        });
    });
});

module.exports = mongoose.model('User', UserSchema);