var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config');

mongoose.connect(config.mongodbUri, { useMongoClient: true });

var db = mongoose.connection;

db.on('error', function (err) {
    console.log('connection error:', err.message);
});
db.once('open', function callback () {
    console.log('Connected to DB!');
});

module.exports = { User: require('./user'), Event: require('./event') };