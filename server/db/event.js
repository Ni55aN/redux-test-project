var mongoose = require('mongoose');

var EventSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        required: true
    },    
    title: {
        type: String,
        required: true
    },
    start: {
        type: Number,
        required: true
    },
    duration: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Event', EventSchema);