import C from '../const';

export function formatTime(num) { // hours
    
    num = num < 13 ? num: num - 12;
    var hours = Math.floor(num);
    var minutes = Math.floor(num % 1 * 60);
        
    return `${hours}:${String(minutes).padStart(2, '0')}`;
}

export function formatEventTime(num) {
    return formatTime(C.START_TIME + num / 60);
}

export function formatEventDuration(num) {
    return formatTime(num / 60);
}

export function parseTime(str) {
    var [hours, minutes] = str.split(':').map(n => parseInt(n));

    return hours * 60 + minutes
}

export function parseEventTime(str) {
    return parseTime(str) - C.START_TIME * 60
}

export function parseEventDuration(str) {
    return parseTime(str)
}