import store from './store';

export default async function (path, data, method = 'get') {

    var opts = {
        method,
        headers: {
            'Content-type': 'application/json',
            'Authorization': store.getState().auth.token
        },
        body: JSON.stringify(data)
    };

    var res = await fetch(path, opts);

    if (res.status === 401) {
        store.dispatch({ type: 'LOGOUT' });
        localStorage.removeItem('token');
        return { status: false, message: 'Need authorization' };
    }
    
    return await res.json();
}