import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import './App.css';

import Calendar from './Calendar/Calendar';
import Header from './Header';
import Login from './Login';
import NotFound from './NotFound';
import Notification from './Notification';

export default class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Switch>
                    <Route exact path="/" component={Calendar} />    
                    <Route path="/login" component={Login} />
                    <Route component={NotFound} />
                </Switch>
                <Notification/>
            </div>    
        );
    }
}
