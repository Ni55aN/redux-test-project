import React from 'react';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { Redirect } from 'react-router'

import request from '../request';
import store from '../store';

import './Login.css'

export default class Login extends React.Component {
    
    state = {
        username: '',
        password: '',
        loggedIn: store.getState().auth.token !== null
    }

    handleChange = (field, e) => {
        this.setState({ [field]: e.target.value });
    }

    onLogin = async () => {
        var { username, password } = this.state;
        var res = await request('/auth/login', { username, password }, 'post');
        
        if (res.success) {
            store.dispatch({ type: 'SET_TOKEN', token: res.token });
            localStorage.setItem('token', res.token);
            this.setState({ loggedIn: true });
        }
        else {
            if (res.errors)
                for (var field in res.errors)
                    store.dispatch({ type: 'NOTIFY', message: res.errors[field] });
            else
                store.dispatch({ type: 'NOTIFY', message: res.message });
        }
    }

    onSignup = async () => {
        var { username, password } = this.state;
        var res = await request('/auth/signup', { username, password }, 'post');
        
        if (res.success)
            store.dispatch({ type: 'NOTIFY', message: 'You have successfully registered' });
        else {
            if (res.errors)
                for (var field in res.errors)
                    store.dispatch({ type: 'NOTIFY', message: res.errors[field] });
            else
                store.dispatch({ type: 'NOTIFY', message: res.message });
        }
    }

    render() {

        return <div>
            <form className="Login">
                <div>
                    <TextField
                        label="Username"
                        onChange={this.handleChange.bind(this, 'username')} />
                </div>
                <div>
                    <TextField
                        label="Password"
                        type="password"
                        onChange={this.handleChange.bind(this, 'password')}
                    /></div>
                <div><Button color="primary"
                    onClick={this.onLogin}>Login</Button></div>
            
                <div><Button
                    onClick={this.onSignup}>Signup</Button></div>
            </form>
            {this.state.loggedIn ?
                <Redirect to='/' /> :
                null}
        </div>    
    }
}