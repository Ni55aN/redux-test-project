import React from 'react';
import { Link } from 'react-router-dom'
import Button from 'material-ui/Button';

import store from '../store';

import './Header.css';

export default class Header extends React.Component {

    state = { loggedIn: store.getState().auth.token !== null}
    
    constructor() {
        super();
        store.subscribe(() => {
            this.setState({loggedIn: store.getState().auth.token !== null})
        })
    }
    
    logout = () => {
        store.dispatch({ type: 'LOGOUT' });
        localStorage.removeItem('token');
        window.location.reload();
    }

    render() {
        return <header className="Header">
            <Link to="/">Calendar</Link>
            {this.state.loggedIn ?
                <a onClick={this.logout} className="userState">Logout</a>
                : 
                <Link to="/login" className="userState">Login</Link>
            }
        </header>
    }
}