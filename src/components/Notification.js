import React from 'react';
import Snackbar from 'material-ui/Snackbar';

import store from '../store';

export default class Notification extends React.Component {
    
    state = { message: null };

    constructor() {
        super();

        store.subscribe(() => {
            this.setState({ message: store.getState().notification })
        });
    }

    handleRequestClose = (e) => {
        this.setState({ message: null });
        store.dispatch({ type: 'CLOSE_NOTIFICATION' });
    }

    render() {
        return <Snackbar
            open={this.state.message !== null}
            autoHideDuration={4000}
            onRequestClose={this.handleRequestClose}
            message={this.state.message || ''} />
    }
}