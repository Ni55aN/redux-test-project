import React from 'react';
import './Day.css';
import C from '../../const';
import store from '../../store';
import Event from './Event';

export default class Day extends React.Component {

    state = {events:[]}

    constructor(props) {
        super();
        this.height = (C.END_TIME - C.START_TIME) / C.STEP_TIME * C.TIME_MARK_HEIGHT;
        this.minutes = Math.ceil((C.END_TIME - C.START_TIME) * 60);

        store.subscribe(() => {
            this.setState({ events: store.getState().events });
        });
    }

    prepareEventsLayout() {
        this.layouts = new Array(this.state.events.length)
            .fill(null).map(() => ({cell: 0, space: 0}));
        
        this.layouts.forEach((l1, i) => {
            this.layouts.forEach((l2, j) => {
                if (i === j) return;
                let e1 = this.state.events[i];
                let e2 = this.state.events[j];
                        
                var e1end = e1.start + e1.duration;
                var e2end = e2.start + e2.duration;
        
                if (e2.start < e1end && e1.start < e2end) {
                    if (l1.cell === 0)
                        l2.cell = 1;
                    l2.space = 1;
                }
            });
        });
    }

    getLayoutStyle(e, l) {
        let top = e.start / this.minutes;
        let height = e.duration / this.minutes;
        let right = l.cell/(l.space+1);
        let width = 1/(l.space+1);

        var style = { top, right, width, height };

        Object.keys(style).forEach(k => {
            style[k] = (style[k] * 100) + '%';
        });
        return style;
    }

    render() {
        this.prepareEventsLayout();

        var events = this.state.events.map((e, i) => {
            return <Event key={i}
                style={this.getLayoutStyle(e, this.layouts[i])}
                data={e}/>
        })
        
        return <div className="Day" style={{ height: this.height }}>{events}</div>
    }
}