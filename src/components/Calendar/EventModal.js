import React from 'react';
import Modal from 'react-modal';
import Button from 'material-ui/Button';

import store from '../../store';
import request from '../../request';
import { formatEventDuration, formatEventTime } from '../../utils';

import './Modal.css'

export default class EventModal extends React.Component {

    constructor(props) {
        super();

        this.state = {
            isOpen: false,
            event: null
        };
        store.subscribe(() => {
            let event = store.getState().event;
            
            this.setState({ isOpen: event !== null, event });
        });
    }

    removeEvent = async ()=> {
        var event = store.getState().event;

        var res = await request(`/api/event/${event._id}`, {}, 'delete');

        if (res.status) {
            store.dispatch({ type: 'REMOVE_EVENT', event });
            this.close();
        }
        else
            store.dispatch({ type: 'NOTIFY', message: 'Error while removing event' });
    }

    open = () => {
        this.setState({isOpen: true});
    }
    
    afterOpen = () => {
    }

    close = () => {
        this.setState({ isOpen: false });
        store.dispatch({ type: 'CLOSE_EVENT' });
    }

    render() {
        if (!this.state.event) return null;

        return <Modal    
            isOpen={this.state.isOpen}
            onAfterOpen={this.afterOpen}
            onRequestClose={this.close}
            ariaHideApp={false}
            contentLabel="Modal">
            <button onClick={this.close} className="close-button">X</button>
            <h2>{this.state.event.title}</h2>
            <p>Start time: {formatEventTime(this.state.event.start)}</p>
            <p>Duration: {formatEventDuration(this.state.event.duration)}</p>
            <p className="handle-buttons">
                <Button color="accent" onClick={this.removeEvent}>Remove</Button>
            </p>    
        </Modal>
    }        
}