import React from 'react';
import _ from 'lodash';
import {formatTime} from '../../utils';
import C from '../../const';
import './Time.css';

export default class Time extends React.Component {

    constructor(props) {
        super();
        this.hours = _.range(C.START_TIME, C.END_TIME+C.STEP_TIME, C.STEP_TIME);
    }

    render() {
        var hours = this.hours.map(t => {
            return <div key={t} style={{height: C.TIME_MARK_HEIGHT+'px'}}
                className={t % 1 === 0 ? 'mark int' : 'mark real'}>
                <div className="time">{formatTime(t)}</div>
            </div>;
        })

        return <div className="Time">{hours}</div>
    }
}