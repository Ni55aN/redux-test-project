import React from 'react';
import Modal from 'react-modal';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';

import request from '../../request';
import store from '../../store';
import { parseEventDuration, parseEventTime } from '../../utils';

import './Modal.css'

var textFieldStyle = {
    width: 200,
    marginRight: 20,
    marginTop: 20
}

var textareaStyle = {
    width: '100%'
}

export default class AddEventModal extends React.Component {

    constructor(props) {
        super();

        this.state = {
            isOpen: false,
            form: {title:'', start:'08:00', duration: '00:30'}
        };
    }

    submit = async (e) => {
        e.preventDefault();
        
        var title = this.state.form.title;
        var start = parseEventTime(this.state.form.start);
        var duration = parseEventDuration(this.state.form.duration);

        var event = { title, start, duration };

        var res = await request('/api/event', event, 'post');

        if (res.status) {
            store.dispatch({ type: 'ADD_EVENT', event: res.event })
            this.close();
        } else {
            if (res.errors)
                for (var field in res.errors)
                    store.dispatch({ type: 'NOTIFY', message: res.errors[field] });
            else
                store.dispatch({ type: 'NOTIFY', message: res.message });
        }
    }

    handleChage = (field, e) => {
        var form = Object.assign({}, this.state.form);

        form[field] = e.target.value;
        this.setState({ form });
    }

    open = () => {
        this.setState({isOpen: true});
    }
    
    afterOpen = () => {
    }

    close = () => {
        this.setState({isOpen: false});
    }

    render() {
        return <Modal    
            isOpen={this.state.isOpen}
            onAfterOpen={this.afterOpen}
            onRequestClose={this.close}
            ariaHideApp={false}
            contentLabel="Modal">
            <button onClick={this.close} className="close-button">X</button>
            <h2>Add event</h2>
            <form onSubmit={this.submit}>
                <TextField
                    id="title"
                    label="Title"
                    value={this.state.form.title}
                    multiline
                    rowsMax="4"
                    onChange={this.handleChage.bind(this, 'title')}
                    style={{ ...textFieldStyle, ...textareaStyle } }
                />    
                <TextField
                    id="time"
                    label="Start"
                    type="time"
                    value={this.state.form.start}
                    onChange={this.handleChage.bind(this, 'start')}
                    style={textFieldStyle}
                    inputprops={{
                        step: 300 // 5 min
                    }}
                />
                <TextField
                    id="time"
                    label="Duration"
                    type="time"
                    value={this.state.form.duration}
                    onChange={this.handleChage.bind(this, 'duration')}
                    style={textFieldStyle}
                    inputprops={{
                        step: 300 // 5 min
                    }}
                />
                <Button raised color="primary" type="submit" >
                    Add
                </Button>
            </form>
        </Modal>
    }        
}