import React from 'react';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import ExIcon from 'material-ui-icons/ImportExport';

import { saveAs } from 'file-saver';

import Day from './Day';
import Time from './Time';
import AddEventModal from './AddEventModal';
import EventModal from './EventModal';

import request from '../../request';
import store from '../../store';

import './Calendar.css';

export default class Calendar extends React.Component {

    async componentDidMount() {

        var res = await request('/api');

        if (res.status)
            store.dispatch({ type: 'SET_EVENTS', events: res.events });
        else
            store.dispatch({ type: 'NOTIFY', message: res.message });
        
    }

    export = () => {
        var events = store.getState().events
            .map(({ title, start, duration }) => ({ title, start, duration }));
        
        var data = JSON.stringify(events);
        var blob = new Blob([data], { type: 'application/json;charset=utf-8' });

        saveAs(blob, 'events.json');
    }

    render() {
        return <div className="Calendar">
            <Time/>
            <Day/>
            <EventModal/>
            <AddEventModal ref={(ref) => this.addModal = ref} />
            <div className="float-buttons">
                <Button fab mini
                    color="primary"
                    title="Export"
                    onClick={this.export}>
                    <ExIcon/>
                </Button>
                <Button
                    fab color="primary"
                    aria-label="add"
                    onClick={() => this.addModal.open()}>
                    <AddIcon />
                </Button>    
            </div>  
        </div>;
    }
}