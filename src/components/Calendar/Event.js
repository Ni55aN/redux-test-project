import React from 'react';
import './Event.css';
import store from '../../store';

export default class Event extends React.Component {

    constructor(props) {
        super();
        this.props = props;
    }

    onClickEvent = () => {
        store.dispatch({ type: 'OPEN_EVENT', event: this.props.data });
    }

    render() {
        return <div 
            className="Event"
            onClick={this.onClickEvent}
            style={this.props.style}>
            {this.props.data.title}
        </div>  
    }
}