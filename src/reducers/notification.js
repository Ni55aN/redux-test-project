export default (state = null, action) => {
    
    switch (action.type) {
    case 'NOTIFY': return action.message
    case 'CLOSE_NOTIFICATION': return null
    default: return state        
    }
}