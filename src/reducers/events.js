export default (state = [], action) => {
    switch (action.type) {
    case 'SET_EVENTS': return action.events;
    case 'ADD_EVENT': return [...state, action.event];
    case 'REMOVE_EVENT': return state.filter(e => e!==action.event);
    default: return state        
    }
}