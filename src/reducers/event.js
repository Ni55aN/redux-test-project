export default (state = null, action) => {

    switch (action.type) {
    case 'OPEN_EVENT': return action.event
    case 'CLOSE_EVENT': return null
    default: return state        
    }
}