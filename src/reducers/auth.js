var localToken = localStorage.getItem('token');

export default (state = { token: localToken }, action) => {
    
    switch (action.type) {
    case 'SET_TOKEN': return { token: action.token }
    case 'LOGOUT': return { token: null }
    default: return state        
    }
}