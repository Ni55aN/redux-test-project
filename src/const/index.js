export default {
    START_TIME: 8,
    END_TIME: 17,
    STEP_TIME: 0.5,
    TIME_MARK_HEIGHT: 50
}